Heroku Connect Skeleton

===================================
Setting up Heroku Connect
===================================

1. Create an account or login at heroku.com

2. Navigate to https://elements.heroku.com/addons#data-store-utilities

3. Click on the Heroku Connect Tile

4. Scroll down and click on the "Deploy to Heroku" button inside the "Heroku Connect Quickstart" tile

5. (Optionally) add a name to the app, and click "Deploy for Free"

6. Click "Manage", and then "Heroku Connect"

7. You should be on the "Create Connection" screen. Leave the defaults as is and click "Next"

8. Authorize the connection to your org through the OAuth page presented.

9. You should now be on a URL that looks like https://connect.heroku.com/sync/5555/overview. It's time to set-up your mapping. Click "Create Mapping".

10. I've set-up this demo to work with the Account object, so select that from the list.

11. Select whichever fields you would like mapped to the Heroku Connect DB. I suggest name, phone, website, etc, and then click "Save".

===================================
Deploying the Sinatra App to Heroku
===================================

1. Open the terminal, and navigate to this repo's directory 

2. Run 'bundle install'

3. Back in your web browser, navigate to https://postgres.heroku.com/databases, and select your Heroku connect database.

3. Create a .env file in this directory, and set the following DB values to the values displayed in the web browser.

EXAMPLE of what the .env should look like:

HOST=ec2-107-22-161-155.compute-1.amazonaws.com 
DBPORT=5432
USERNAME=test123
PASSWORD=dbpass123
DATABASE=dbname123

3. Back in the terminal, set the same values on Heroku by entering the following: (remember to replace the values with the values from your DB!):

heroku config:set HOST=ec2-107-22-161-155.compute-1.amazonaws.com DBPORT=5432 USERNAME=test123 PASSWORD=dbpass123 DATABASE=dbname123

4. Force push to Heroku by running:

heroku git:remote -a <yourherokuappname>
git push heroku master -f

5. Navigate to http://<herokuapname>.herokuapp.com to see the app!

===================================
Adding Custom Models (Not Sync'd to Salesforce)
===================================


Create the model by running:

rake generate:model NAME=<Modelname>

i.e.

rake generate:model NAME=Festival

===================================

Create the migration by running:

rake generate:migration NAME=create_<pluralofmodelname>

i.e.

rake generate:migration NAME=create_festivals

===================================

Run the migration by running:

rake db:migrate

Now you're ready to create/save your new model!


===================================
Connect to Heroku DB Remotely
===================================

psql -h <host> -p <port> -U <username> -W <database>

i.e.

psql -h ec2-107-22-161-155.compute-1.amazonaws.com -p 5432 -U test123 -W dbname123