
get '/' do
  @accounts = Account.all
  # render :json => @account.to_json
  erb :accounts
end

post '/accounts' do
  p params
  @account = Account.new(params)
  @account.save
  redirect '/'
end

delete '/accounts/:id' do
  Account.find(params[:id]).delete
  redirect '/'
end
