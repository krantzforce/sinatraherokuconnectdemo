class CreateFestivals < ActiveRecord::Migration
  def change
  	create_table :festivals do |t|
  	  t.string :name
  	  t.string :url

  	  t.timestamps
  	end
  end
end
