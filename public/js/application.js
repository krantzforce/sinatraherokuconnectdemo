  // This is called after the document has loaded in its entirety
  // This guarantees that any elements we bind to will exist on the page
  // when we try to bind to them

  // See: http://docs.jquery.com/Tutorials:Introducing_$(document).ready()
  
$(document).ready(function() {
  $('.delete').on("click", function(e){
  	$.ajax({
        url: '/accounts/' + $(e.target).data().id,
        type: 'DELETE',
        success: function(){
        	window.location.reload();
        }
    });
  })
});
